def count_men(exfile, search_str='men'):
    num = 0
    with open(exfile, 'r') as f:
        out1 = f.readlines()
        for s in out1:
            num += s.count(search_str)

    print('The number of men is: ', num)

    return num

def capitalize_women(exfile):
    with open(exfile, 'r') as f:
        lines = f.read().splitlines()

    outlist = []

    for line in lines:
        outlist.append(line.replace('woman', 'WOMAN'))

    outfile = open('example_text_new.txt', 'w')
    outfile.write("\n".join(outlist))

def contains_blue_devil(exfile):
    """ Prints 'Blue Devil contained!' if Blue Devil found. Otherwise, does nothing

    """
    with open(exfile, 'r') as f:
        lines = f.read().splitlines()

    for line in lines:
        if 'Blue Devil' in line:
            print('Blue Devil contained!')
            return 'Duke Article'


def find_non_terminal_said(exfile):
    import re

    f = open(exfile)
    line = f.readline()
    while(line != ''):
        line = re.compile('said ')
        print(said.search(line))
        line = file.readline()

    f.close()

        
if __name__ == '__main__':
    # count_men('example_text.txt')
    # capitalize_women('example_text.txt')
    contains_blue_devil('example_text.txt')
